# -*- coding: utf-8 -*-
from math import sqrt, isnan
import matplotlib.pyplot as plt
from sklearn import metrics

import csv
import operator
import os
import random

import config

class KNN(object):
    def __init__(self, k, training_set, testing_set):
        self.k = k
        self.header_file = config.HEADER
        self.trainingSet = training_set
        self.testSet = testing_set
        self.measures = {
            'accuracy': 0,
            'precision': 0,
            'recall': 0,
            'fmeasure': 0,
            'auc': 0,
            'specificity': 0,
        }

    def euclideanDistance(self,instance1, instance2, length):
        distance = 0
        for x in range(1,length):
            distance += pow((float(instance1[x]) - float(instance2[x])), 2)
        return sqrt(distance)

    def getNeighbors(self,testInstance):
        distances = []
        length = len(testInstance)
        for x in range(1,len(self.trainingSet)):
            dist = self.euclideanDistance(testInstance, self.trainingSet[x], length)
            distances.append((self.trainingSet[x], dist))
        distances.sort(key=operator.itemgetter(1))
        neighbors = []
        for x in range(0,self.k):
            neighbors.append(distances[x][0])
        return neighbors

    def getResponse(self,neighbors):
        classVotes = {}
        for x in range(0, len(neighbors)):
            response = neighbors[x][0]
            if response in classVotes:
                classVotes[response] += 1
            else:
                classVotes[response] = 1
        sortedVotes = sorted(classVotes.iteritems(), key=operator.itemgetter(1), reverse=True)
        return sortedVotes[0][0]

    def calculateAssessmentMeasures(self,predictions):
        TP = FP = FN = TN = 0
        actual = list()
        predicted = list()
        for x in range(len(self.testSet)):
            if self.testSet[x][0] == 'Activating' and predictions[x] != 'Activating':
                actual.append(1)
                predicted.append(0)
                FN += 1
            elif self.testSet[x][0] == 'Inactivating' and predictions[x] != 'Inactivating':
                actual.append(0)
                predicted.append(1)
                FP += 1
            elif self.testSet[x][0] == 'Activating' and predictions[x] == 'Activating':
                actual.append(1)
                predicted.append(1)
                TP += 1
            else:
                actual.append(0)
                predicted.append(0)
                TN += 1
        self.measures['accuracy'] = round(float((TP + TN))/(TP + TN + FP + FN), 4)
        self.measures['precision'] = round(float(TP)/(TP + FP), 4)
        self.measures['recall'] = round(float(TP)/(TP + FN), 4)
        try:
            self.measures['specificity'] = round(float(TN)/(TN + FP), 4)
        except ZeroDivisionError:
            self.measures['specificity'] = 0
        self.measures['fmeasure'] = round((2*(self.measures['precision']*self.measures['recall']))/(self.measures['precision']+ self.measures['recall']), 4)

        FP_rate, TP_rate, thresholds = metrics.roc_curve(actual, predicted)
        auc = metrics.auc(FP_rate, TP_rate)

        self.measures['auc'] = auc

        plt.title('Receiver Operating Characteristic')
        plt.plot(FP_rate, TP_rate, 'b', label='AUC = %0.2f'% auc)
        plt.legend(loc='lower right')
        plt.plot([0,1],[0,1],'r--')
        plt.xlim([-0.1,1.2])
        plt.ylim([-0.1,1.2])
        plt.ylabel('True Positive Rate')
        plt.xlabel('False Positive Rate')
        plt.savefig('tests/roc_curve_{}.png'.format(os.getpid()))

        return True
