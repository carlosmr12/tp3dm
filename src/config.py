K = int()
DBFILE = str()
KFOLDS = int()

AVERAGE_MEASURES = {
    'accuracy': int(),
    'precision': int(),
    'recall': int(),
    'fmeasure': int(),
    'auc': int(),
    'specificity': int(),
}
