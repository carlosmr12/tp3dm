# -*- coding: utf-8 -*-
"""
TP3 – Técnicas de Classificação Aplicadas a Base de Dados de Mutações em Proteínas Cinases
==========================================================================================

Carlos Henrique Miranda Rodrigues
chmrodrigues@ufmg.br

DCC917 – Mineração de Dados
Departamento de Ciência da Computação
Universidade Federal de Minas Gerais - UFMG
"""

from argparse import ArgumentParser
from sklearn import cross_validation
import csv
import numpy as np
import os

from knn import KNN
import config

def main():
    dataset_activating = list()
    dataset_inactivating = list()
    with open (config.DBFILE, 'rb') as csvfile:
        reader = csv.reader(csvfile)
        config.HEADER = reader.next()
        for line in reader:
            if line[0] == 'Activating':
                dataset_activating.append(line)
            else:
                dataset_inactivating.append(line)
    dataset_activating = np.array(dataset_activating)
    dataset_inactivating = np.array(dataset_inactivating)

    kf_activating = cross_validation.KFold(n=len(dataset_activating), n_folds=config.KFOLDS, shuffle=True)
    kf_inactivating = cross_validation.KFold(n=len(dataset_inactivating), n_folds=config.KFOLDS, shuffle=True)

    folds_counter = 1
    for (train_index_activating, test_index_activating),(train_index_inactivating, test_index_inactivating) in zip(kf_activating, kf_inactivating):
        training = dataset_activating[train_index_activating].tolist() + dataset_inactivating[train_index_inactivating].tolist()
        testing = dataset_activating[test_index_activating].tolist() + dataset_inactivating[test_index_inactivating].tolist()

        knn = KNN(config.K, training, testing)

        predictions = list()
        with open('tests/statistics_{}_fold_{}.csv'.format(folds_counter, os.getpid()), 'ab') as stats_file:
            writer = csv.writer(stats_file, delimiter=';')
            for x in range(len(knn.testSet)):
                neighbors = knn.getNeighbors(knn.testSet[x])
                result = knn.getResponse(neighbors)
                predictions.append(result)

                writer.writerow([knn.testSet[x][0],result])

        knn.calculateAssessmentMeasures(predictions)
        for key,value in knn.measures.iteritems():
            config.AVERAGE_MEASURES[key] += value
        folds_counter += 1

    for key,value in config.AVERAGE_MEASURES.iteritems():
        print "{}: {}".format(key,value/config.KFOLDS)

if __name__ == '__main__':
    parser = ArgumentParser(description=u'TP3 – Técnicas de Classificação Aplicadas a Base de Dados de Mutações em Proteínas Cinases - DCC917')

    parser.add_argument('k', metavar='k', default=3, nargs='?', type=int, help=u'K distance for KNN algorithm')
    parser.add_argument('dbfile', metavar='dbfile', default=u'fixtures/dataset.csv', nargs='?', type=str, help=u'.csv database file')
    parser.add_argument('kfold', metavar='kfold', default=10, nargs='?', type=int, help=u'KFold parameter for cross-validation')

    args = parser.parse_args()

    config.K = args.k
    config.DBFILE = args.dbfile
    config.KFOLDS = args.kfold

    main()
