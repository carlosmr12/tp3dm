# TP3 – Técnicas de Classificação Aplicadas a Base de Dados de Mutações em Proteínas Cinases
######Carlos Henrique Miranda Rodrigues
######chmrodrigues@ufmg.br

######DCC917 – Mineração de Dados
######Departamento de Ciência da Computação
######Universidade Federal de Minas Gerais - UFMG

##Requirements
1. I used virtualenvwrapper to help working with dependencies and I strongly recommend you to do the same ([Virtualenvwrapper installation](http://virtualenvwrapper.readthedocs.org/en/latest/install.html#basic-installation));
2. `pip install -r requirements.txt`

##Usage
`python main.py k dbfile kfold`
* k - K distance for the KNN algorithm (default 3);
* dbfile - .csv file with the mutations we are working with (default fixtures/dataset.csv);
* kfold - Number of folds for cross validation (default 10);
